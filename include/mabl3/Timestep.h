/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * C++ Timestep  https://gitlab.com/mabl3/cpp-timestep
 * ---------------------------------------------------
 *
 * MIT License
 *
 * Copyright (c) 2020 Matthis Ebel
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

#ifndef TIMESTEP_H
#define TIMESTEP_H

#include <chrono>
#include <iostream>

namespace mabl3 {

//! Easily introduce time measurement into code
class Timestep {
public:
    //! Constants to specify desired time resolution in output
    enum unit {nanoseconds, microseconds, milliseconds, seconds, minutes, hours};

    //! c'tor
    /*! \param description String to describe for what process the time is measured */
    Timestep(std::string const & description, bool silent = false)
        : description_{description},
          end_{std::chrono::system_clock::now()},
          ended_{false}, silent_{silent},
          start_{std::chrono::system_clock::now()} { 
        if (!silent_) {   // no output in silent mode
            std::cout << "Starting '" << description << "'" << std::endl; 
        }
    }
    //! Return the elapsed time from construction until call of end() in desired resolution
    auto elapsed(Timestep::unit unit) const {
        auto now = (ended_) ? end_ : std::chrono::system_clock::now();
        switch(unit) {
            case nanoseconds : return std::chrono::duration_cast<std::chrono::nanoseconds>(now - start_).count();
            case microseconds : return std::chrono::duration_cast<std::chrono::microseconds>(now - start_).count();
            case milliseconds : return std::chrono::duration_cast<std::chrono::milliseconds>(now - start_).count();
            case seconds : return std::chrono::duration_cast<std::chrono::seconds>(now - start_).count();
            case minutes : return std::chrono::duration_cast<std::chrono::minutes>(now - start_).count();
            case hours : return std::chrono::duration_cast<std::chrono::hours>(now - start_).count();
            default : return std::chrono::duration_cast<std::chrono::seconds>(now - start_).count();    // suppress compiler warning
        }
    }
    //! Stop time measuring
    void end() {
        end_ = std::chrono::system_clock::now();
        ended_ = true;
    }
    //! Stop time measuring and print the elapsed time, automatically chose the resolution
    void endAndPrint() {
        end();
        print();
    }
    //! Stop time measuring and print the elapsed time with the desired resolution
    void endAndPrint(Timestep::unit unit) {
        end();
        print(unit);
    }
    //! Returns \c true if time measuring was stopped
    bool ended() const { return ended_; }
    //! Print the elapsed time, automatically chose the resolution
    void print() const {
        auto now = (ended_) ? end_ : std::chrono::system_clock::now();
        auto ns = std::chrono::duration_cast<std::chrono::nanoseconds>(now - start_).count();
        auto mus = std::chrono::duration_cast<std::chrono::microseconds>(now - start_).count();
        auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now - start_).count();
        auto s = std::chrono::duration_cast<std::chrono::seconds>(now - start_).count();
        auto m = std::chrono::duration_cast<std::chrono::minutes>(now - start_).count();
        if (ns < 10000) { print(nanoseconds); }
        else if (mus < 10000) { print(microseconds); }
        else if (ms < 10000) { print(milliseconds); }
        else if (s <= 180) { print(seconds); }
        else if (m <= 180) { print(minutes); }
        else { print(hours); }
    }
    //! Print the elapsed time with the desired resolution
    void print(Timestep::unit unit) const {
        auto now = (ended_) ? end_ : std::chrono::system_clock::now();
        if (silent_) { return; } // no output in silent mode
        std::cout << description_ << " (took ";
        switch(unit) {
            case nanoseconds : std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(now - start_).count() << " ns)";
            break;
            case microseconds : std::cout << std::chrono::duration_cast<std::chrono::microseconds>(now - start_).count() << " us)";
            break;
            case milliseconds : std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(now - start_).count() << " ms)";
            break;
            case seconds : std::cout << std::chrono::duration_cast<std::chrono::seconds>(now - start_).count() << " s)";
            break;
            case minutes : std::cout << std::chrono::duration_cast<std::chrono::minutes>(now - start_).count() << " min)";
            break;
            case hours : std::cout << std::chrono::duration_cast<std::chrono::hours>(now - start_).count() << " h)";
            break;
        }
        std::cout << std::endl;
    }
    //! Returns \c true if time measuring has not stopped yet
    bool running() const { return (!ended_); }

private:
    std::string description_;
    std::chrono::time_point<std::chrono::system_clock> end_;
    bool ended_;
    bool silent_;
    std::chrono::time_point<std::chrono::system_clock> start_;
};

} // NAMESPACE mabl3

#endif // TIMESTEP_H
