# Cpp Timestep

Small library to measure and print elapsed time between certain points in your 
program.

## Installation

Copy the `Timestep.h` from `include/mabl3` into your project and 
`#include "mabl3/Timestep.h"`

## Usage

Initialize a `Timestep` object with a short description of what you are 
measuring right before the respective part of your code starts executing. After
that code is finished, call one of the methods below to stop measuring time and,
if desired, print out how much time elapsed.

```c++
auto ts = mabl3::Timestep("Measure important stuff");

// ... do your important stuff ...

ts.end();   // just stop measuring time
// OR
ts.endAndPrint(); // stop measuring and print the elapsed time in an appropriate
                  // resolution
// OR
ts.endAndPrint(mabl3::Timestep::milliseconds); // stop measuring and print the
                                               // elapsed time in milliseconds
```

Not that each of aboves calls will stop the time measuring. You can check the
elapsed time in between with the following approach:

```c++
auto ts = mabl3::Timestep("Measure important stuff");

// ... do your important stuff ...

auto elapsed = ts.elapsed(mabl3::Timestep::milliseconds); // get the number of
                                                          // ticks elapsed in
                                                          // the desired
                                                          // resolution
                                                          
// ... continue important stuff ...

ts.end();

std::cout << "The first part of important stuff took " << elapsed << " ms"; 
std::cout << "Important stuff took " << ts.elapsed(mabl3::Timestep::seconds) 
    << " s in total";
```

Note that the second call to `ts.elapsed()` gives the same result as 
`ts.print()` or the likes as time measuring was stopped by `ts.end()` before.

If you want to print the measured time later, call `ts.print()` either without
arguments or with the desired resolution.

Available resolutions are
* mable::Timestep::nanoseconds
* mable::Timestep::microseconds
* mable::Timestep::milliseconds
* mable::Timestep::seconds
* mable::Timestep::minutes
* mable::Timestep::hours

You can check if the time measuring is still running with the self-explanatory
`ts.running()` or `ts.ended()` methods.