#include <chrono>
#include <thread>

#include <mabl3/Timestep.h>

int main() {
    auto ts = mabl3::Timestep("Waiting 3ms");
    std::this_thread::sleep_for(std::chrono::milliseconds(3));
    ts.end();
    ts.print(mabl3::Timestep::milliseconds);

    ts = mabl3::Timestep("Waiting another ms");
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    ts.endAndPrint();

    ts = mabl3::Timestep("Waiting another 2 ms or so");
    std::this_thread::sleep_for(std::chrono::milliseconds(2));
    if (ts.running()) { std::cout << "still waiting..." << std::endl; }
    std::cout << "Time elapsed: " << ts.elapsed(mabl3::Timestep::nanoseconds) << " us" << std::endl;
    if (!ts.ended()) { std::cout << "continue waiting..." << std::endl; }
    ts.endAndPrint();

    std::cout << "Silent mode, no output after this line" << std::endl;
    ts = mabl3::Timestep("Invisible description", true);
    ts.endAndPrint();

    return 0;
}
